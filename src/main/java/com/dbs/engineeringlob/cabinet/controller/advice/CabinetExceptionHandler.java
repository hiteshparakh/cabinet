package com.dbs.engineeringlob.cabinet.controller.advice;

import java.io.FileNotFoundException;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class CabinetExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler({FileNotFoundException.class})
	protected ResponseEntity<Object> handleFileNotFoundException(Exception exc, WebRequest request) {
		StringBuilder bodyOfResponse = new StringBuilder()
				.append("Given ").append(request.getDescription(false).contains("file") ? "file" : "directory")
				.append(" not found on the disk");
        return handleExceptionInternal(exc, bodyOfResponse.toString(), 
          new HttpHeaders(), HttpStatus.NOT_FOUND, request);
	}
	
	@ExceptionHandler({Exception.class})
	protected ResponseEntity<Object> handleAllOtherExceptions(Exception exc, WebRequest request) {
		String bodyOfResponse = "Something went wrong, pls try later.";
        return handleExceptionInternal(exc, bodyOfResponse, 
          new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
	}
}
