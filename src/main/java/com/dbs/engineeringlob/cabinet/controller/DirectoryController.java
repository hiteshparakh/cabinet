package com.dbs.engineeringlob.cabinet.controller;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dbs.engineeringlob.cabinet.domain.DirectoryDetails;
import com.dbs.engineeringlob.cabinet.service.FileInfoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/dir")
@Api(value="CabinetApp", description="Operations pertaining to directory listing in Cabinet app") 
public class DirectoryController {

	private static final Logger logger = LoggerFactory.getLogger(DirectoryController.class);
	
	@Autowired
	private FileInfoService fileInfoService;
	
	@ApiOperation(value = "Recursively list files within the given directory path", response = DirectoryDetails.class)
	@RequestMapping(value = "/listDetails", method = RequestMethod.GET)
	public ResponseEntity<DirectoryDetails> listAllFilesWithDetails(@RequestParam(value = "dirPath") String dirPath) throws IOException {
		
		logger.debug("Retrieving directory listing for :" + dirPath);
		
		return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(fileInfoService.listDirectoryDetails(dirPath));
	}
}
