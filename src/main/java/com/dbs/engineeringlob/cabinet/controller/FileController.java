package com.dbs.engineeringlob.cabinet.controller;

import java.io.FileNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dbs.engineeringlob.cabinet.domain.File;
import com.dbs.engineeringlob.cabinet.service.FileInfoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/file")
@Api(value="CabinetApp", description="Operations pertaining to file information in Cabinet app") 
public class FileController {

	private static final Logger logger = LoggerFactory.getLogger(FileController.class);
	
	@Autowired
	private FileInfoService fileInfoService;

	@ApiOperation(value = "Provides the complete information about the given file", response = File.class)
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public ResponseEntity<File> getFileInfo(@RequestParam(value = "filePath") String filePath) throws FileNotFoundException {
    	
    	logger.debug("Retrieving file information for :" + filePath);
    	
    	return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(fileInfoService.retrieveFileInfo(filePath));
    }
    
}
