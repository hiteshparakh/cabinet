package com.dbs.engineeringlob.cabinet.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.dbs.engineeringlob.cabinet.domain.DirectoryDetails;
import com.dbs.engineeringlob.cabinet.domain.File;

@Service
public class FileInfoServiceImpl implements FileInfoService {

	private static final Logger logger = LoggerFactory.getLogger(FileInfoServiceImpl.class);
	
	@Override
	public DirectoryDetails listDirectoryDetails(String dirPath) throws IOException {
		
		logger.debug("Building directory details for path :" + dirPath);
		
		DirectoryDetails directoryDetails = new DirectoryDetails();
		List<File> allFiles = new ArrayList<File>();

		try (Stream<Path> paths = Files.walk(Paths.get(dirPath))) {
			
			allFiles = paths.map(path -> {
				java.io.File file = path.toFile();

				File fileObj = buildFileObject(file);
				
				return fileObj;
				
			}).collect(Collectors.toList());
			
		} catch (NoSuchFileException nsfe) {
			logger.error("Failed to locate the given dir :" + dirPath);
			throw new FileNotFoundException();
		} catch (IOException e) {
			logger.error("Error while processing the dir :" + dirPath);
			throw e;
		}

		directoryDetails.setInputDirectoryPath(dirPath);
		directoryDetails.setFiles(allFiles);
		directoryDetails.setCountOfFiles(allFiles.size());

		return directoryDetails;
	}

	@Override
	public File retrieveFileInfo(String filePath) throws FileNotFoundException {
		
		logger.debug("Fetching details for file :" + filePath);
		
		java.io.File file = Paths.get(filePath).toFile();
		if(file.exists())
			return buildFileObject(file);
		else { 
			logger.error("Failed to locate the given file :" + filePath);
			throw new FileNotFoundException();
		}
	}
	
	private File buildFileObject(java.io.File file) {
		File fileObj = new File();
		
		fileObj.setDirectory(file.isDirectory());
		fileObj.setAbsPath(file.getAbsolutePath());
		fileObj.setName(file.getName());
		fileObj.setParentName(file.getParent());
		fileObj.setSize(file.length());
		fileObj.setCanExecute(file.canExecute());
		fileObj.setCanRead(file.canRead());
		fileObj.setCanWrite(file.canWrite());
		fileObj.setHidden(file.isHidden());
		
		return fileObj;
	}

}
