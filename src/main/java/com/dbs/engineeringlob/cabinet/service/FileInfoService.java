package com.dbs.engineeringlob.cabinet.service;

import java.io.FileNotFoundException;
import java.io.IOException;

import com.dbs.engineeringlob.cabinet.domain.DirectoryDetails;
import com.dbs.engineeringlob.cabinet.domain.File;

public interface FileInfoService {

	DirectoryDetails listDirectoryDetails(String dirPath) throws IOException;
	
	File retrieveFileInfo(String filePath) throws FileNotFoundException;

}
