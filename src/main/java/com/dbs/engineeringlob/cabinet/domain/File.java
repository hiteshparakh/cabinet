package com.dbs.engineeringlob.cabinet.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class File {

	String name;
	String parentName;
	String absPath;
	boolean isDirectory;
	boolean canExecute;
	boolean canRead;
	boolean canWrite;
	boolean isHidden;
	long size;
}
