package com.dbs.engineeringlob.cabinet.integration;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.dbs.engineeringlob.cabinet.TestBase;

@AutoConfigureMockMvc
public class FileControllerTest extends TestBase {

	@Autowired
	private MockMvc mockMvc;

    @Test
    public void retrieveFileInfoSuccess() throws Exception {
    	
    	ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get("/file/info").param("filePath", "C:\\Orders\\Vizio.pdf"));
    	
    	resultActions.andExpect(MockMvcResultMatchers.status().isOk())
    		.andExpect(MockMvcResultMatchers.jsonPath("name").value("Vizio.pdf"))
    		.andExpect(MockMvcResultMatchers.jsonPath("absPath").value("C:\\Orders\\Vizio.pdf"))
    		.andExpect(MockMvcResultMatchers.jsonPath("directory").value(false))
    		.andExpect(MockMvcResultMatchers.jsonPath("canExecute").value(true))
    		.andExpect(MockMvcResultMatchers.jsonPath("canRead").value(true))
    		.andExpect(MockMvcResultMatchers.jsonPath("canWrite").value(true))
    		.andExpect(MockMvcResultMatchers.jsonPath("hidden").value(false))
    		.andExpect(MockMvcResultMatchers.jsonPath("size").value(172335));
    }

    @Test
    public void retrieveFileInfoFailure() throws Exception {
    	
    	ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get("/file/info").param("filePath", "C:\\Orders\\badfile.pdf"));
    	
    	resultActions.andExpect(MockMvcResultMatchers.status().isNotFound());
    }
}
