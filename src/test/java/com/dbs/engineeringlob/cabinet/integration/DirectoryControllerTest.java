package com.dbs.engineeringlob.cabinet.integration;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.dbs.engineeringlob.cabinet.TestBase;

@AutoConfigureMockMvc
public class DirectoryControllerTest extends TestBase {
	
	@Autowired
	private MockMvc mockMvc;

    @Test
    public void retrieveDirectoryListingSuccess() throws Exception {
    	
    	ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get("/dir/listDetails").param("dirPath", "c:\\Intel"));
    	
    	resultActions.andExpect(MockMvcResultMatchers.status().isOk())
    		.andExpect(MockMvcResultMatchers.jsonPath("inputDirectoryPath").value("c:\\Intel"))
    		.andExpect(MockMvcResultMatchers.jsonPath("countOfFiles").value(5));
    }

    @Test
    public void retrieveDirectoryListingFailure() throws Exception {
    	
    	ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get("/dir/listDetails").param("dirPath", "c:\\baddirectory"));
    	
    	resultActions.andExpect(MockMvcResultMatchers.status().isNotFound());
    }
}
